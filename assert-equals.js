function assertEquals(expect, actual) {

        // Check type
        if (!isMatchedType(expect, actual)) {
            throw new Error('Expected type ' + typeOf(expect) + ' but found type ' + typeOf(actual));
        }

        // Check values
        if (!isMatched(expect, actual)) {

            switch (typeOf(expect)) {
                case "string":
                case "number":
                case "object":
                    throw new Error('Expected ' + formatValueForErrorMessage(expect) + ' but found ' + formatValueForErrorMessage(actual));
                    return false;
                    break;
                case "array":
                    // Check array length
                    if (!isMatchedArrayLength(expect, actual)) {
                        throw new Error('Expected array length ' + expect.length + ' but found ' + actual.length);
                        // Check array values- we already know they don't match.
                    } else {
                        throw new Error(generateArrayErrorMessage(expect, actual));
                    }
                    return false;
                    break;
                default:
                    throw new Error("Unhandled exception");
                    return false;
            }
        } else {
            return true;
        }
}


// Type functions
function typeOf(obj) {
    return {}.toString.call(obj).split(' ')[1].slice(0, -1).toLowerCase();
}

// Array functions
function compareArrays(expect, actual) {
    if (expect.length !== actual.length) return false;
    for (var i = 0, len = expect.length; i < len; i++) {
        if (expect[i] !== actual[i]) {
            return false;
        }
    }
    return true;
}

function buildArrayAsString(arr) { // We need a "[3,4,'1']" string for [3,4,'1'], not "[3,4,1] as obj.toString() returns.
    let output = "[";

    for (let i = 0; i < arr.length; i++) {
        if (typeOf(arr[i]) == "string") {
            output += "'" + arr[i] + "'" + ",";
        } else if (typeOf(arr[i]) == "array") {
            output += buildArrayAsString(arr[i]) + ",";
        } else {
            output += arr[i] + ",";
        }
    }
    return output.substring(0, output.length - 1) + "]";
}

// Formatting functions
function generateArrayErrorMessage(expect, actual) {
    let errorMessage = "";
    let nonMatchingElements = getNonMatchingArrayElements(expect, actual);

    for (let i = 0; i < nonMatchingElements.length; i++) {
        let el = nonMatchingElements[i];


        let expectText = formatValueForErrorMessage(el.expect);
        let actualText = formatValueForErrorMessage(el.actual);
        errorMessage += 'Expected ' + expectText + ' but found ' + actualText + "\r\n";
    }

    return errorMessage;

}

function formatValueForErrorMessage(val) {
    let output = "";
    switch (typeOf(val)) {
        case "string":
        case "number":
        case "object":
            return typeOf(val) == 'string' ? "'" + val + "'" : val;
            break;
        case "array":
            return buildArrayAsString(val);
            break;

            return output;
    }
}

// Match functions
function getNonMatchingArrayElements(expect, actual) {
    let map = {};
    expect.forEach(i => map[i] = false);
    actual.forEach(i => map[i] === false && (map[i] = true));
    let checkedArray = Object.keys(map).map((k, f) => ({
        expect: expect[f],
        actual: actual[f],
        matched: isMatched(expect[f], actual[f])
    }));
    let nonMatchingObjects = checkedArray.filter(function (el) {
        return !el.matched;
    });
    return nonMatchingObjects;
}

function isMatchedArrayLength(expect, actual) {
    return expect.length == actual.length;
}

function isMatchedType(expect, actual) {
    let expectedType = typeOf(expect);
    let actualType = typeOf(actual);
    return expectedType == actualType;
}

function isMatched(expect, actual) {
    let expectedType = typeOf(expect);
    let isMatched = false;

    if (expectedType == "array") {
        isMatched = compareArrays(expect, actual);
    } else {
        isMatched = expect === actual;
    }

    return isMatched;

}
